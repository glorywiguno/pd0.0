import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// import { RouterModule, Routes } from '@angular/router';

import { RoutingModule } from './routing.module';
import { AppComponent } from './app.component';
import { NavBarComponent } from './shared/nav-bar/nav-bar.component';
import { LandingMainComponent } from './landing/landing-main/landing-main.component';
import { SliderComponent } from './landing/slider/slider.component';
import { TeamViewComponent } from './landing/team-view/team-view.component';
import { ActivitiesViewComponent } from './landing/activities-view/activities-view.component';
import { MemberListComponent } from './landing/team-view/member-list/member-list.component';
import { MemberCardComponent } from './landing/team-view/member-card/member-card.component';
import { NgxCarouselModule } from 'ngx-carousel';

@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    LandingMainComponent,
    SliderComponent,
    TeamViewComponent,
    ActivitiesViewComponent,
    MemberListComponent,
    MemberCardComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RoutingModule,
    NgxCarouselModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
