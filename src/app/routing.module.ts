// module for routing

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LandingMainComponent } from './landing/landing-main/landing-main.component';

// for now only 1 routes
const routes : Routes = [
  { path: '', component: LandingMainComponent }
]

@NgModule({
  imports: [ RouterModule.forRoot(routes)],
  exports: [ RouterModule ]
})

export class RoutingModule {}
