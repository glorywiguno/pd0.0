import { Component, OnInit, EventEmitter, Output, Input} from '@angular/core';
import { NgxCarousel } from 'ngx-carousel';


@Component({
  selector: 'slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.css']
})

export class SliderComponent implements OnInit {

  @Output() onImageClicked: EventEmitter<any> = new EventEmitter<any>();
  @Input() images:any;

  // public photos; 
  // public curIndex;
  



  constructor() {
  //   this.photos = [
  //     {
  //       path: "/assets/images/slide1.jpg",
  //       title: "abc"
  //     },
  //     {
  //       path: "/assets/images/splash1.jpg",
  //       title: "abc2"
  //     },
  //     {
  //       path: "/assets/images/splash2.jpg",
  //       title: "abv3"
  //     },
  //     {
  //       path: "/assets/images/team.jpg",
  //       title: "abce3"
  //     }

  //   ];
  //   this.curIndex=0;
   }

 public carouselTileItems: Array<any>;
  public carouselTile: NgxCarousel;
 
  ngOnInit(){
    this.carouselTileItems = this.images;
 
    this.carouselTile = {
      grid: {xs: 2, sm: 3, md: 3, lg: 5, all: 0},
      slide: 2,
      speed: 400,
      animation: 'lazy',
      point: {
        visible: true
      },
      load: 2,
      touch: true,
      easing: 'ease'
    }
  }
 
  public carouselTileLoad(evt: any) {
 
    const len = this.carouselTileItems.length
    if (len <= 30) {
      for (let i = len; i < len + 10; i++) {
        this.carouselTileItems.push(i);
      }
    }
 
  }


  onImageClickedFn(image:any): void{
    this.onImageClicked.emit(image);

  }

  // nextSlide() {
  //   this.curIndex = (this.curIndex + 1) % this.photos.length
  //   // console.log(this.curIndex);
  // }

  // prevSlide() {
  //   // cari cara yang lebih pinter ya nanti
  //   if(this.curIndex !== 0) {
  //     this.curIndex = this.curIndex - 1;
  //   }
  //   else{
  //     this.curIndex = this.photos.length - 1;
  //   }

  //   // console.log(this.curIndex);
  // }
}
