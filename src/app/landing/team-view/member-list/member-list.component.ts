import { Component, OnInit, Input } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations' ;

import { Member } from '../member';

@Component({
  selector: 'member-list',
  templateUrl: './member-list.component.html',
  styleUrls: ['./member-list.component.scss'],

  // NOTE:
  // on animation, using max height from 0 to 100% or 0 to * does not work
  // height form 0 to 100% either, but from 0 to * works
  animations: [
    trigger('listState',[
      state('inactive', style({
        height: '0',
        opacity: '0'
        })
      ),
      state('active', style({
        height: '*',
        opacity: '1'
      })
      ),
    transition('inactive <=> active', animate('600ms ease-in-out'))
    ])
  ]
})


export class MemberListComponent implements OnInit {
  // variables are made public because there is problem with deploying angular
  public members : Member[];
  public state: string = 'inactive';
  constructor() {
    this.members = [
      {
        photo: "/assets/images/team/set2.jpg",
        name: "Andreas Setiawan",
        position: "Koordinator PD",
        contact:{
          facebook: 'https://www.facebook.com/andreas.setiawan.hartanto',
          insta: 'https://www.instagram.com/andreas.setiawan/?hl=en',
          email: 'mailto:andreasset2@gmail.com'
        }
      },
      {
        photo: "/assets/images/team/jewon.jpg",
        name: "Jessica N. Wondany",
        position: "Acara 1",
        contact:{
          facebook: 'https://www.facebook.com/jessicanataliaa',
          insta: 'https://www.instagram.com/jnataliaw/?hl=en',
          email: ''
        }
      },
      {
        photo: "/assets/images/team/ipeh.jpg",
        name: "Olivia M. Sugiarto ",
        position: "Acara 2",
        contact:{
          facebook: 'https://www.facebook.com/genovevaolivia',
          insta: 'https://www.instagram.com/genovevaolivia/?hl=en',
          email: ''
        }
      },
      {
        photo: "/assets/images/team/lele.jpg",
        name: "Leonora Natasha",
        position: "Bendahara",
        contact:{
          facebook: 'https://www.facebook.com/leonoranm',
          insta: 'https://www.instagram.com/leonoranatasha/?hl=en',
          email: ''
        }
      },
      {
        photo: "/assets/images/team/kedo.jpg",
        name: "Kevin A. Theonardo",
        position: "Bazaar dan Acara Rekreasi",
        contact:{
          facebook: 'https://www.instagram.com/kevinandrient/?hl=en',
          insta: 'https://www.instagram.com/kevinandrient/?hl=en',
          email: ''
        }
      },
      {
        photo: "/assets/images/team/regi.jpg",
        name: "Maria Regina",
        position: "Choir",
        contact:{
          facebook: 'https://www.facebook.com/regina.jasmine.96',
          insta: 'https://www.instagram.com/reginajasmine97/?hl=en',
          email: ''
        }
      },
      {
        photo: "/assets/images/team/mira.jpg",
        name: "Mira Herliani",
        position: "Doa",
        contact:{
          facebook: '',
          insta: 'https://www.instagram.com/mira_0292/?hl=en',
          email: ''
        }
      },
      {
        photo: "/assets/images/team/cindy.jpg",
        name: "Cindy D. Hadisaputra",
        position: "Liturgi",
        contact:{
          facebook: '',
          insta: 'https://www.instagram.com/cindyhadiputra/?hl=en',
          email: ''
        }
      },
      {
        photo: "/assets/images/team/irvan.jpg",
        name: "Ignatius Irvan",
        position: "Pemerhati",
        contact:{
          facebook: '',
          insta: 'https://www.instagram.com/ignatiusirvan/?hl=en',
          email: ''
        }
      },
      {
        photo: "/assets/images/team/diptya.jpg",
        name: "Diptya Prawatya",
        position: "Perlengkapan",
        contact:{
          facebook: '',
          insta: 'https://www.instagram.com/diptya.prawatya/?hl=en',
          email: ''
        }
      },
      {
        photo: "/assets/images/team/aldvin.jpg",
        name: "Aldvin Tumbelaka",
        position: "Praise and Worship",
        contact:{
          facebook: '',
          insta: 'https://www.instagram.com/aldvintumbelaka/?hl=en',
          email: ''
        }
      },
      {
        photo: "/assets/images/team/glenda.jpg",
        name: "Gabriela Glennda",
        position: "Publikasi",
        contact:{
          facebook: '',
          insta: 'https://www.instagram.com/gabrielaglennda/?hl=en',
          email: ''
        }
      },
      {
        photo: "/assets/images/team/jovic.jpg",
        name: "Jocelyn Victoria",
        position: "Koordinator MPR",
        contact:{
          facebook: 'https://www.facebook.com/jvictoria96',
          insta: '',
          email: ''
        }
      },
      {
        photo: "/assets/images/team/bone.jpg",
        name: "Romo Boni Buahendri",
        position: "Chaplain KKI",
        contact:{
          facebook: 'https://www.facebook.com/boni.buahendri',
          insta: '',
          email: ''
        }
      },
      {
        photo: "/assets/images/team/richard.jpg",
        name: "Richard Oei, Lee Lian Oei",
        position: "Parents Couple",
        contact:{
          facebook: '',
          insta: '',
          email: ''
        }
      }
    ];

  }

  toggleState() {
    this.state = this.state === 'active' ? 'inactive' : 'active';
    // console.log(this.state)
  }
  ngOnInit(){

  }
}
