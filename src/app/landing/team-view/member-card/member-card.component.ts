import { Component, OnInit, Input } from '@angular/core';

import { Member } from '../member';

@Component({
  selector: 'member-card',
  templateUrl: './member-card.component.html',
  styleUrls: ['./member-card.component.scss'],

  inputs: ['person']
})
export class MemberCardComponent implements OnInit {

  @Input() person: Member;
  public dtlSwitch = 'name';
  constructor() {

  }


  ngOnInit() {

  }

  // simple checks for the existence of contacts
  checkContact() {
    return (this.checkMail() || this.checkFb() || this.checkInsta());
  }


  checkMail() {
    return this.person.contact.email !== ''
  }
  checkFb(){
    return this.person.contact.facebook !== ''
  }

  checkInsta() {
    return this.person.contact.insta !== ''
  }

}
