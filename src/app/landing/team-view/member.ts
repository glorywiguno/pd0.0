// class for member,

export class Member {
  // member photo
  photo: string;
  // member name
  name: string;
  // member position in PDKKI Team
  position: string;
  // mungkin nanti tambah contact fb, insta dll
  contact:{
    facebook: string;
    insta: string;
    email: string;
  }
}
