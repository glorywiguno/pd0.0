import { Component, OnInit } from '@angular/core';
import { AfterViewInit, ViewChild } from '@angular/core';
import { MemberListComponent } from './member-list/member-list.component';

@Component({
  selector: 'team-view',
  templateUrl: './team-view.component.html',
  styleUrls: ['./team-view.component.scss']
})
export class TeamViewComponent implements OnInit {
  @ViewChild(MemberListComponent) memberList: MemberListComponent;

  public memberShow : boolean;
  public btnMsgTeam : String;
  

  constructor() {
    this.memberShow = false;
    this.btnMsgTeam = "Meet the team";
  }

  showTeam() {
    this.memberShow = !this.memberShow;
    this.memberList.toggleState();
    if (this.memberShow){
      this.btnMsgTeam = "Close";
    }
    else {
      this.btnMsgTeam = "Meet the team";
    }
  }
  ngAfterViewInit() {

  }
  ngOnInit() {
  }

}
