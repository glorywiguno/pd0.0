import { Component, OnInit } from '@angular/core';
import { ActivitiesViewComponent } from '../activities-view/activities-view.component';


@Component({
  selector: 'landing-main',
  templateUrl: './landing-main.component.html',
  styleUrls: ['./landing-main.component.scss']
})
export class LandingMainComponent implements OnInit {
  public aboutText: string;
  public teamImg: string;
  public bannerLogo: string;

  constructor() {
    this.teamImg = "/assets/images/team2.jpg";
    this.bannerLogo = "/assets/images/PDKKI-logo_white.png"
  }

  ngOnInit() {
  }

}
