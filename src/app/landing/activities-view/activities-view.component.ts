import { Component, OnInit } from '@angular/core';
import { SliderComponent } from '../slider/slider.component';
import { Activity } from './object/activity'; 

@Component({
	selector: 'activities-view',
	templateUrl: './activities-view.component.html',
	styleUrls: ['./activities-view.component.css']
})
export class ActivitiesViewComponent implements OnInit {


	activities = [ 

	new Activity ("/assets/images/slide1.jpg", "abc","lalala"),
	new Activity ("/assets/images/splash1.jpg", "abc2","lalala2"),
	new Activity ("/assets/images/splash2.jpg", "abv3","lalala3"),
	new Activity ("/assets/images/team.jpg", "abce3","lalala4"),



	];

	currentActivity:Activity = this.activities[0];


	constructor() { }

	ngOnInit() {

		
		console.log(this.currentActivity);


	}


	setCurrentActivity( selectedActivity: any){
		this.currentActivity = selectedActivity;
	}

}
