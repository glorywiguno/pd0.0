import { Component, OnInit, Input, OnChanges, HostListener, Inject} from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations' ;
import { DOCUMENT } from '@angular/platform-browser'


@Component({
  selector: 'nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css'],
  animations: [
    trigger('fixNav',[
      state('unfix', style({
        display: 'none',
        transform:'translateY(-100%)'

        })
      ),
      state('fixed', style({

        transform:'translateY(0%)',

      })
      ),
    transition('unfix <=> fixed', animate('200ms linear'))
    ])
  ]
})

export class NavBarComponent implements OnInit {
  public logo : string;
  public state: string;

  constructor(@Inject(DOCUMENT) private document:Document) {

  }

  ngOnInit() {
    this.logo = "/assets/images/PDKKI-logo_black.png";
    this.state = "unfix";
  }

  toggleFix() {
    this.state = this.state === 'active' ? 'inactive' : 'active';
  }


  @HostListener("window:scroll",[])
  onWindowScroll() {
    let scrollAmount = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;
    // console.log(scrollAmount);

    if (scrollAmount > 50) {
      this.state = "fixed";

      // console.log(this.state)
    }
    else {
      this.state = "unfix";
      // console.log(this.state)
    }
  }


}
