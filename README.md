# PD0.0 - Trial


## Current file structure plan

current plan is to make a folder to group each component, module and services
according to the feature/page

 app
  |- activities
  |- landing
  |- shared
  |- team

+ activities
  for activities page components
+ landing
  for landing page components
+ shared
  for shared components and services
+ team
  for team page components

## Milestones
+ completing the bare html/css structure for each page
+ implement page routing capabilities
+ anchoring for landing page
+ slideshow/carousel for activities photo section in landing page
+ activities gallery slideshow
+ more js and animation for page interaction

## Issues
+ developer still learning angular 2/4 structure



## angular-cli documentation

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.4.5.

### Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

### Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

### Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

### Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

### Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

### Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
